//soal 1
var nilai = 100;

if (nilai >= 85) {
    console.log("A");
}
else if (nilai >= 75 && nilai < 85) {
    console.log("B");
}
else if (nilai >= 65 && nilai < 75) {
    console.log("C");
}
else if (nilai >= 55 && nilai < 65) {
    console.log("D");
}
else {
    console.log("E");
}

console.log("");

//soal 2
var tanggal = 4;
var bulan = 10;
var tahun = 2002;

switch (bulan) {
    case 1: { bulan = "Januari" } break
    case 2: { bulan = "Februari" } break
    case 3: { bulan = "Maret" } break
    case 4: { bulan = "April" } break
    case 5: { bulan = "Mei" } break
    case 6: { bulan = "Juni" } break
    case 7: { bulan = "Juli" } break
    case 8: { bulan = "Agustus" } break
    case 9: { bulan = "September" } break
    case 10: { bulan = "Oktober" } break
    case 11: { bulan = "November" } break
    case 12: { bulan = "Desember" } break
    default: { bulan = "Unknow" }
}

console.log(tanggal + " " + bulan + " " + tahun);

console.log("");

//soal 3

console.log("n=3");
for (var n = "#"; n.length <= 3; n += "#") {
    console.log(n);
}

console.log("");

console.log("n=7");
for (var n = "#"; n.length <= 7; n += "#") {
    console.log(n);
}

console.log("");

//soal 4
var m = 10;
var k1 = "I love programming";
var k2 = "I love Javascript";
var k3 = "I love VueJS";
var batas = "";

for (no = 1; no <= m; no++) {
    if (no % 3 == 1) {
        console.log(no + " - " + k1);
        batas += "=";
    }
    else if (no % 3 == 2) {
        console.log(no + " - " + k2);
        batas += "=";
    }
    else if (no % 3 == 0) {
        console.log(no + " - " + k3);
        batas += "=";
        console.log(batas);
    }
}
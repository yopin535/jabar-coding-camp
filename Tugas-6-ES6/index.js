//soal 1
const kelilingPersegi = (s1 = 5, s2 = 5, s3 = 5, s4 = 5) => {
    return s1 + s2 + s3 + s4;
}

console.log(kelilingPersegi());

//soal 2

const newFunction = function literal(firstName, lastName) {
    return {
        fullName: function () {
            console.log(firstName + " " + lastName)
        }
    }
}

newFunction("William", "Imoh").fullName();

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby)

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]

console.log(combined)

//soal 5
const planet = "earth"
const view = "glass"

const theString = `Lorem ${view} dolor sit amet, consectetur adipiscing, ${planet}`

console.log(theString)


//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();
for (i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
}

console.log("===================");
//soal 2

function introduce() {
    return ("Nama saya " + data.name + ", " + "umur saya " + data.age + " tahun" + ", " + "alamat saya di " + data.address + ", " + "dan saya punya hobby yaitu " + data.hobby);
}
var data = { name: "Yopi", age: 19, address: "Sukabumi", hobby: "Gamink" }

var perkenalan = introduce(data);
console.log(perkenalan);

console.log("===================");
//soal 3

function hitung_huruf_vokal(vokal) {
    var jumlah = 0;
    var hurufVokal = ["A", "I", "U", "E", "O", "a", "i", "u", "e", "o"];
    for (var i = 0; i < vokal.length; i++) {
        if (hurufVokal.indexOf(vokal[i]) > -1) {
            jumlah++;
        }
    }
    return jumlah;

}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

console.log("===================");
//soal 4

function hitung(angka) {
    return angka * 2 - 2;
}
console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8

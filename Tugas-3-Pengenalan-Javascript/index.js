//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var up = kedua.substring(8, 18);

console.log(pertama.substring(0, 19) + kedua.substring(0, 8) + up.toUpperCase());

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var sepuluh = parseInt(kataPertama);
var dua = parseInt(kataKedua);
var empat = parseInt(kataKetiga);
var enam = parseInt(kataKeempat);

console.log((sepuluh - enam) * (dua + empat));

//soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 32);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);